import React, { Component } from 'react'
import Web3 from "web3";

export default class web3Class extends Component {
  constructor(provider) {
    this.provider = provider;
    this.web3Instant = new Web3(this.provider);
  }

  async getAccounts() {
    const { web3Instant } = this.state
    try {
      // Get user's Ethereum public address
      const address = (await web3Instant.eth.getAccounts())[0];

      return address;
    } catch (error) {
      return error;
    }
  }

  async getBalance() {
    const { web3Instant } = this.state
    try {
      // Get user's Ethereum public address
      const address = (await web3Instant.eth.getAccounts())[0];

      // Get user's balance in ether
      const balance = web3Instant.utils.fromWei(
        await web3Instant.eth.getBalance(address) // Balance is in wei
      );

      return balance;
    } catch (error) {
      return error;
    }
  }

  // async signMessage() {
  //   const { web3Instant } = this.state
  //   try {
  //     // Get user's Ethereum public address
  //     const fromAddress = (await web3Instant.eth.getAccounts())[0];

  //     const originalMessage = "YOUR_MESSAGE";

  //     // Sign the message
  //     const signedMessage = await web3Instant.eth.personal.sign(
  //       originalMessage,
  //       fromAddress,
  //       "test password!" // configure your own password here.
  //     );

  //     return signedMessage;
  //   } catch (error) {
  //     return error;
  //   }
  // }

  // async getPrivateKey() {
  //   const { provider } = this.state
  //   try {
  //     const privateKey = await provider.request({
  //       method: "eth_private_key",
  //     });

  //     return privateKey;
  //   } catch (error) {
  //     return error;
  //   }
  // }
}
