import React, { createContext, useContext, useState } from 'react'
import { Web3Auth } from '@web3auth/modal';
import { CHAIN_NAMESPACES } from '@web3auth/base';


const Web3AuthContext = createContext();
export const useWeb3Auth = () => useContext(Web3AuthContext)

const clientId = process.env.REACT_APP_WEB3AUTH_CLIENT_ID
function Web3AuthProvider({ children }) {
  const [web3auth, setWeb3auth] = useState(null);
  const [provider, setProvider] = useState(null);

  React.useEffect(() => {
    const init = async () => {
      try {

        const web3auth = new Web3Auth({
          clientId,
          chainConfig: {
            chainNamespace: CHAIN_NAMESPACES.EIP155,
            chainId: process.env.REACT_APP_WEB3AUTH_CHAIN_ID,
            rpcTarget: process.env.REACT_APP_WEB3AUTH_RPC_TARGET, // This is the public RPC we have added, please pass on your own endpoint while creating an app
          },
        });

        setWeb3auth(web3auth);

        await web3auth.initModal();
        if (web3auth.provider) {
          setProvider(web3auth.provider);
        };
      } catch (error) {
        console.error(error);
      }
    };

    init();
  }, []);

  const login = async () => {
    if (!web3auth) {
      console.log("web3auth not initialized yet");
      return;
    }
    const web3authProvider = await web3auth.connect();
    setProvider(web3authProvider);
  };

  const logout = async () => {
    if (!web3auth) {
      console.log("web3auth not initialized yet");
      return;
    }
    await web3auth.logout();
    setProvider(null);
  };

  const contextProvider = {
    web3auth, provider, login, logout
  }
  return <Web3AuthContext.Provider value={contextProvider}>{children}</Web3AuthContext.Provider>;
}

export default Web3AuthProvider