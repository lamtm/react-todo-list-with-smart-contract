import React from 'react'
import { useWeb3Auth } from '../../contexts'

function HomePage() {
  const { provider } = useWeb3Auth()
  return (
    <div>HomePage
      <div className="grid">{provider ? "loggedIn" : "unloggedIn"}</div>
    </div>
  )
}

export default HomePage