import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import { useWeb3Auth } from '../../contexts';

function LoginPage() {
  const { login, provider } = useWeb3Auth()
  const navigate = useNavigate();

  useEffect(() => {
    if (provider) {
      navigate('/');
    } else {
      login()
    }
  }, [provider])

  return (
    <div>LoginPage</div>
  )
}

export default LoginPage