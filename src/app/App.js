import { Navigate } from 'react-router-dom';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Layout from '../components/Layout';
import LoginPage from '../components/Login';
import NotFound from '../components/NotFound';
import { useWeb3Auth } from '../contexts';
import HomePage from '../features/Home';
import './App.css';

function App() {
  const { provider } = useWeb3Auth()

  return (
    <BrowserRouter>
      <Routes >
        <Route path="/" element={provider ? <Layout /> : <Navigate to="/login" />}>
          <Route index element={<HomePage />} />
          <Route path='/' element={<HomePage />} />
        </Route>
        <Route path="/login" element={<LoginPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
